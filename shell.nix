{ pkgs ? import <nixpkgs> { } }:
let
  poetryEnv = pkgs.poetry2nix.mkPoetryEnv {
    projectDir = ./.;
    python = pkgs.python3;
  };
in pkgs.mkShell {
  name = "env";
  buildInputs = with pkgs; [ bashInteractive gcc poetryEnv ];
}

