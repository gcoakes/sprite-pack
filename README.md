# sprite-pack

sprite-pack is a utility which packs multiple sprites into sheet image and
metadata file. It supports the following input sprites:

- Animated, either column or row
- Static
- Packed static, either column or row

Animated and static sprites will be directly copied into the output sheet.
Frame width and direction will be encoded into the metadata file for animated
sprites. Packed static sprites will be extracted from the input image and
packed individually into the output sheet.
